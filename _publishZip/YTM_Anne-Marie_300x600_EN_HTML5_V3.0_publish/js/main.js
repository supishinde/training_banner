'use strict';
~ function() {
     var 	 sineOut = Sine.easeOut,
			 copyTwoContainer = document.getElementById('copyTwoContainer'),
			 copyThreeContainer = document.getElementById('copyThreeContainer'),
			 copyFourContainer = document.getElementById('copyFourContainer'),
			 bgExit = document.getElementById("bgExit"),
   		 ad = document.getElementById("mainContent");


	 window.init = function () {
		 assignClass(copyTwoContainer, 'copyTwo');
		 assignClass(copyThreeContainer, 'copyThree');
		 assignClass(copyFourContainer, 'copyFour');
		 bgExit.addEventListener('click',exitEventHandler)			
		 
		var tl = new TimelineMax();
	    tl.set(ad, { perspective: 1000, force3D: true })

	 tl.addLabel("frameOne")
	 	 tl.to(["#copyOne","#playIcon"], 0.8, { opacity:1, ease:sineOut }, "frameOne")
   		 tl.to(["#googlePlayLogo","#appStoreLogo"], 0.8, { opacity:1, ease:sineOut })

	tl.addLabel("frameTwo","+=1.5")
		 tl.to(["#copyOne","#playIcon","#googlePlayLogo","#appStoreLogo"], 0.8, { opacity:0, ease:sineOut }, "frameTwo")
		 tl.to("#albumBg", 2.7, { scale: 0.833, opacity: 1, rotation: 0.01, ease: sineOut}, "frameTwo+=0.4")

		 tl.set("#YTMusicLogo", { opacity: 1, ease: sineOut },"frameTwo+=0.8")
		 tl.set("#theCopy", {x:-14, opacity: 1 },"frameTwo+=1.5")    
         tl.to("#theCopy", 0.5,{ x:0, ease: sineOut },"frameTwo+=1.5")

		 tl.to([".copyTwoLine", "#searchIcon"], .5, { opacity: 1 }, "frameTwo+=2")
		

		 tl.to("#textCursor",0.3,{opacity:1,delay:0.5, yoyo:true, repeat:2},"frameTwo+=1.7")
		 tl.to("#textCursor",0.8,{opacity:1,x:224, ease:SteppedEase.config(15)},"frameTwo+=2.7")

		 tl.add(function () { typeAnimation('.copyTwo') }, "frameTwo+=2.5")

		 tl.to("#textCursor",0.3,{opacity:0},"-=0.2")
		 tl.set("#isHereCopy", {x:-14, opacity: 1 },"-=0.2")	
		 tl.to("#isHereCopy", 0.5,{ x:0, ease: sineOut },"-=0.2")	

    tl.addLabel("frameThree","+=1.7")
		 tl.to("#albumBg", 0.8, { opacity:0,perspective: 1000, ease:sineOut }, "frameThree")
		 tl.to(["#theCopy", "#isHereCopy", "#copyTwoContainer", ".copyTwoLine"], 0.8, { opacity:0, ease:sineOut }, "frameThree")
		 tl.to("#musicVideoBg", 2.7, { scale: 0.833, opacity: 1, rotation: 0.01, ease: sineOut },"frameThree+=0.4")

		 tl.set("#theCopy", {x:-14, opacity: 1 },"frameThree+=1.5")    
         tl.to("#theCopy", 0.5,{ x:0, ease: sineOut },"frameThree+=1.5")

		 tl.set("#searchIcon", {opacity:1, x: 37 }, "frameThree+=2" )
		 tl.to([".copyThreeLineOne"], .5,{ opacity: 1 }, "frameThree+=2" )
		 tl.to([".copyThreeLineTwo"], .1,{ opacity: 1 }, "frameThree+=3.2" )

 		 tl.set("#textCursor", {x:38}, "frameThree+=1.7")	
 		 tl.to("#textCursor",0.3,{opacity:1,delay:0.5, yoyo:true, repeat:2},"frameThree+=2")
		 tl.to("#textCursor",0.68,{opacity:1,x:179, ease:SteppedEase.config(18)},"frameThree+=2.6")
		 
		  tl.set("#textCursor", {opacity:0}, "frameThree+=3.1")	
		 tl.set("#textCursor", {opacity:1,x:37,y:39}, "frameThree+=3.1")
		  tl.to("#textCursor",0.68,{opacity:1,x:184, ease:SteppedEase.config(18)},"frameThree+=3.1")


		 tl.add(function () { typeAnimation('.copyThree') }, "frameThree+=2.5");

		 tl.to("#textCursor",0.3,{opacity:0},"-=0.3")
		 tl.set("#isHereCopy", {y:36, x:-14, opacity: 1 },"-=0.1")	
   		 tl.to("#isHereCopy", 0.5, {x:0, ease:sineOut },"-=0.1")

	tl.addLabel("frameFour","+=1.7")
		 tl.to("#musicVideoBg", 0.8, { opacity:0, perspective: 1000, ease:sineOut }, "frameFour")
		 tl.to(["#theCopy", "#isHereCopy","#copyThreeContainer",".copyThreeLine"], 0.8, { opacity:0, ease:sineOut }, "frameFour")
		 tl.to("#performanceBg", 2.7, { scale: 0.833, opacity: 1, rotation: 0.01, ease: sineOut },"frameFour+=0.4")

		 tl.set("#theCopy", {x:-14, opacity: 1 },"frameFour+=1.5")    
         tl.to("#theCopy", 0.5,{ x:0, ease: sineOut },"frameFour+=1.5")

		 tl.set("#searchIcon", {opacity:1, x: 39,y:3 }, "frameFour+=2")
		 tl.to(".copyFourLineOne", .5, {opacity:1}, "frameFour+=2")
		 tl.to(".copyFourLineTwo", .1, {opacity:1}, "frameFour+=3.2")

		 tl.set("#textCursor", {x:40,y:1}, "frameFour+=1.7")	
 		 tl.to("#textCursor",0.3,{opacity:1,delay:0.5, yoyo:true, repeat:2},"frameFour+=2")
		 tl.to("#textCursor",0.68,{opacity:1,x:179, ease:SteppedEase.config(20)},"frameFour+=2.6")

		  tl.set("#textCursor", {opacity:0}, "frameFour+=3.1")	
		 tl.set("#textCursor", {opacity:1,x:9,y:39}, "frameFour+=3.3")
		  tl.to("#textCursor",0.68,{opacity:1,x:213, ease:SteppedEase.config(25)},"frameFour+=3.3")

		 tl.add(function () { typeAnimation('.copyFour') }, "frameFour+=2.5")

		 tl.set("#textCursor", {opacity:0}, "-=0.1")		 	
     	 tl.set("#isHereCopy",{y:36, x:-14, opacity: 1 },"-=0.2")
	   	 tl.to("#isHereCopy", 0.5, {x:0, opacity:1, ease:sineOut },"-=0.2")
		
	tl.addLabel("frameFive","+=1.7")
		tl.to("#theCopy", 0.5, { opacity: 0, ease: sineOut }, "frameFive");
		tl.to(["#performanceBg", "#isHereCopy", ".copyFourLineOne", ".copyFourLineTwo", copyFourContainer], 0.8, { opacity: 0, ease: sineOut }, "frameFive");
		tl.set("#playIcon",  { top:2, left:20, scale:0.65 },"frameFive")
		tl.to(".mask", .75, { height: 146, ease:sineOut},"frameFive+=0.1")

		tl.to(["#copyFive","#playIcon", "#googlePlayAppStore"], 0.8, { opacity: 1, ease: sineOut }, "frameFive+=0.1")
		tl.to("#YTMusicLogo", 0.8, { top:537, left:14, scale:0.83 },"frameFive+=0.1")
	
	
	}



	function assignClass(elem, className) {
		var numberOfPath = elem.getElementsByTagName('path');


		for (var i = 0; i < numberOfPath.length; i++) {
			elem.getElementsByTagName('path')[i].setAttribute('class', className);
		}
	}

	function typeAnimation(elem) {
		var tl = new TimelineMax({})
		tl.to("#searchIcon", .4, { opacity: 0 },0)
		tl.staggerTo(elem, 0.05, { opacity: 1, ease: Back.easeIn }, 0.05,0.2);

	}
	function exitEventHandler(e){
		e.preventDefault();
		window.open(window.clickTag)
	}
}();

window.onload = init();